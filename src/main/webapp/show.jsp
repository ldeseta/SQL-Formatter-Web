<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="keywords" content="sql, format, sql formatter, web sql formatter, sql beautifier" />
        <meta name="description" content="SQL Formatter is a free Java utility that formats SQL statments to create a more readable script." />
        <title>Web SQL Formatter</title>
        <link rel="icon" type="image/x-icon" href="favicon.png" />

        <link href="js/libs/twitter-bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/webformatter.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${root}/">Web SQL Formatter</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="${root}/">Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <h3>Now your SQL is pretty!</h3>
            <textarea id="output" class="form-control" rows="30" wrap="off"><c:out value="${sql}"/></textarea>
            <br/>
            <a class="btn btn-primary" href="${root}/"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</a>
            <button id="select-all" class="btn btn-default" href="${root}">Select all</button>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/libs/jquery/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/libs/twitter-bootstrap/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#output").select();
                $("#select-all").click(function () {
                    $("#output").select();
                });
            });
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-77189846-1', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>