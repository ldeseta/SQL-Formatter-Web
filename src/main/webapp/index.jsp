<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="keywords" content="sql, format, sql formatter, web sql formatter, sql beautifier" />
        <meta name="description" content="SQL Formatter is a free Java utility that formats SQL statments to create a more readable script." />
        <title>Web SQL Formatter</title>
        <link rel="icon" type="image/x-icon" href="favicon.png" />

        <link href="js/libs/twitter-bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/webformatter.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${root}/">Web SQL Formatter</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="${root}/">Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="page-header">
                <h1>Web SQL Formatter <small>Make your SQL scripts readable by humans</small></h1>
            </div>

            <c:if test="${not empty errorMessage}">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Oh snap! Check your syntax and try again.</strong> Only insert statements and comments are supported.<br/>${errorMessage}
                </div>
            </c:if>

            <form action="format" method="POST" accept-charset="ISO-8859-1">
                <div class="form-group">
                    <textarea class="form-control" id="sql" name="sql" placeholder="Paste your SQL script here" rows="15" wrap="off" autofocus="on" required="required"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Make it pretty!</button>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-default btn-sm" id="sample-data">Load sample SQL</button>
                    </div>
                </div>
            </form>


            <hr>
            <h4>Known issues and limitations</h4>
            <ul>
                <li>Only INSERT statements and comments are supported.</li>
                <li>Comments are ignored and removed.</li>
            </ul>

            <h4>About Web SQL Formatter</h4>
            Web SQL Formatter is a simple frontend for <a href="https://gitlab.com/ldeseta/SQL-Formatter">SQL Formatter</a>, a free Java utility
            that formats SQL statments to create a more readable script.

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/libs/jquery/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/libs/twitter-bootstrap/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#sample-data").click(function () {
                    var sql = "-- Some sample insert statements\n\n";
                    sql += "insert into movie (id, title, release_date, enabled) values (1, 'Cloud Atlas', '2009-12-11', true);\ninsert into genre (id, description) values (1, 'Drama'), (2, 'Terror'), (3, 'Action');\ninsert into movie (id, title, release_date) values (2, 'The Place Beyond The Pines', '2011-11-04');\ninsert into genre (id, description) values (4, 'Comedy'); ";
                    $("#sql").val(sql);
                    return false;
                });
            });
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-77189846-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>